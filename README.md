# README #

This Blender addon bakes the material diffuse colors of all selected objects into vertex colors. This only works with BI materials.

### Installation ###
Install like any other Blender addon. Check the Blender manual if you are unsure. After installation, the addon can be found in the Properties panel (N-panel) of the 3D viewport.