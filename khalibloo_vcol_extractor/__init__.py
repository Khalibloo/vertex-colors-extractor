# <pep8 compliant>
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Khalibloo VCol Extractor",
    "version": (1, 0),
    "author": "Khalifa Lame",
    "blender": (2, 77, 0),
    "description": "Extracts vertex colors from material diffuse colors",
    "location": "3D Viewport > Properties(N) Panel > VCol Extractor",
    "category": "Khalibloo"}

import bpy
from . import ops

#============================================================================
# DRAW PANEL
#============================================================================

class KhaliblooVColExtractorPanel(bpy.types.Panel):
    """Creates a Panel in the properties context of the 3D viewport"""
    bl_label = "VCol Extractor"
    bl_idname = "khalibloo_vcol_extractor_panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'


    def draw(self, context):
        layout = self.layout
        scene = context.scene
        
        layout.operator("object.khalibloo_material_color_to_vcol", text="Extract")