import bpy
import bmesh


def main(context, obj):
    if(len(obj.data.vertex_colors) < 1):
        return
    print(obj.name)
    context.scene.render.use_bake_to_vertex_color = True
    context.scene.render.bake_type = 'TEXTURE'
    context.scene.render.use_bake_selected_to_active = False
    
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.context.scene.objects.active = obj
    bpy.ops.object.bake_image()
    #bpy.ops.object.mode_set(mode='EDIT')
    #bm = bmesh.from_edit_mesh(obj.data)
    #for face in bm.faces:
    #    matIndex = face.material_index
    #    matCol = obj.material_slots[matIndex].material.diffuse_color
    #    
    #bpy.ops.object.mode_set(mode='OBJECT')


class MatColToVcol(bpy.types.Operator):
    """Set vertex colors from material colors. 
    You must select a vertex color set first"""
    bl_idname = "object.khalibloo_material_color_to_vcol"
    bl_label = "Mat color to Vcolor"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        for obj in context.selected_objects:
            if(obj.type == 'MESH'):
                main(context, obj)
        return {'FINISHED'}


def register():
    bpy.utils.register_class(MatColToVcol)


def unregister():
    bpy.utils.unregister_class(MatColToVcol)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.object.khalibloo_material_color_to_vcol()
